from django.contrib import admin
from .models.huespedes import Huespedes

# se registra el modelo creado para que pueda ser usado por la aplicacion
admin.site.register(Huespedes)

