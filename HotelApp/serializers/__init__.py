# se importa el serializador creado para que los demas codigos puedan importarla
from .huespedesSerializer import HuespedesSerializer
from .habitacionesSerializer import HabitacionesSerializer
from .checkInSerializer import CheckInSerializer