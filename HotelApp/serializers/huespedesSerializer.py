# se importa el modelo de producto para usarlo en el serializador 
from HotelApp.models.huespedes import Huespedes
# se importa la clase serializers para crear clase serializadora
from rest_framework import serializers

# se crea clase serializadora para poder convertir los datos del modelo a JSON
class HuespedesSerializer(serializers.ModelSerializer):
    class Meta:
        # se asigna el modelo a la clase
        model = Huespedes
        # se eligen cuales campos del modelo se desea mostar
        fields = ['id','nombre','apellido','documento','pais','email','telefono','direccion']