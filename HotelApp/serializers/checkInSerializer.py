from HotelApp.models.huespedes      import Huespedes
from HotelApp.models.habitaciones   import Habitaciones
from HotelApp.models.checkIn        import CheckIn
from rest_framework                 import serializers


class CheckInSerializer(serializers.ModelSerializer):
    class Meta:
        model  = CheckIn
        fields = ["id", "huesped", "habitacion", "register_date", "note"]

    def to_representation(self, obj):
        checkIn     = CheckIn.objects.get(id=obj.id)
        huespedes  = Huespedes.objects.get(id=checkIn.huesped_id)
        
        habitaciones = Habitaciones.objects.get(id=checkIn.habitacion_id)
        return {
            'numero_checkIn'           :    checkIn.id,
            'register_date':    checkIn.register_date,
            'note'         :    checkIn.note,
            'huesped': {
                'documento':    huespedes.documento,
                'nombre'   :    huespedes.nombre,
                'apellido' :    huespedes.apellido
            },
            'habitacion':{
                'numero_habitacion'   : habitaciones.numero_habitacion,
                'numero_camas'        : habitaciones.numero_camas
            }
            
        }

