#from django.db import models
#from HotelApp.models import habitaciones
from HotelApp.models.habitaciones  import Habitaciones
#from HotelApp.models.huespedes     import Huespedes
from rest_framework                import serializers

class HabitacionesSerializer(serializers.ModelSerializer):
    class Meta:
        model =Habitaciones
        fields =[ 'id','numero_habitacion', 'numero_camas', 'descripcion', 'precio', 'observaciones']
    

