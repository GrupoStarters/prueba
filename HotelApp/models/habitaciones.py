# clase importada para creacion de modelo
from django.db import models

# clase creada para el modelo de producto
class Habitaciones(models.Model):
    #atributos del producto, que van a representar las columnas de la tabla
    id                  = models.AutoField(primary_key=True)
    numero_habitacion   = models.IntegerField(default=0, unique=True)
    numero_camas        = models.IntegerField(default=0,)
    descripcion         = models.CharField('descripcion', max_length = 100)
    precio              = models.IntegerField(default=0, unique=True)
    observaciones       = models.CharField('observaciones', max_length = 100)

   
   