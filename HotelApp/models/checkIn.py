from django.db import models
from .huespedes  import Huespedes
from .habitaciones  import Habitaciones

class CheckIn(models.Model):
    id              = models.AutoField(primary_key=True)
    huesped         = models.ForeignKey(Huespedes, related_name="huesped", on_delete=models.CASCADE)
    habitacion      = models.ForeignKey(Habitaciones, related_name="habitacion", on_delete=models.CASCADE)
    register_date   = models.DateTimeField(auto_now_add=True, blank=True)
    note            = models.CharField(max_length=100)