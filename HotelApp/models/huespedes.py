# clase importada para creacion de modelo
from django.db import models

# clase creada para el modelo de producto
class Huespedes(models.Model):
    #atributos del producto, que van a representar las columnas de la tabla
    id          = models.AutoField(primary_key=True)
    nombre      = models.CharField('NameHueped', max_length = 30)
    apellido    = models.CharField('ApellidoHueped', max_length = 30)
    documento   = models.IntegerField(default=0, unique=True)
    pais        = models.CharField('PaisHueped', max_length = 30)
    email       = models.EmailField('EmailHueped', max_length = 50, unique=True)
    telefono    = models.IntegerField(default=0)
    direccion   = models.CharField('DireccionHueped', max_length = 30)
   