# se importan para poder generar las respuestas http
from rest_framework.response import Response
# se importa para poder consumir las vistas de ayuda 
from rest_framework.views import APIView
# se importa para poder modificar las vistas de ayuda
from rest_framework.decorators import api_view
#se importa para poder usar el modelo creado
from HotelApp.models import Huespedes, huespedes
# se importa para poder manipular el modelo con el serializador creado
from HotelApp.serializers import HuespedesSerializer

#se agrega el modificador a la funcion para restringir los metodos http permitidos
@api_view(['GET','POST'])
def huespedes_api_view(request):

    # se filtran los mensajes enviados por GET
    if request.method == 'GET':
        #  usando ORM se realiza la consultaa la base de datos de todos los productos
        huespedes = Huespedes.objects.all()
        # se representa la informacion solicitada en JSON y se marca many=true para asignar anidacion
        huespedes_serializer = HuespedesSerializer(huespedes,many=True)
        # se envida la respuesta serializada que se encuentra en data
        return Response(huespedes_serializer.data)

    # se filtran los mensajes enviados por POST
    elif request.method == 'POST':
        # se extraen los datos enviados en la peticion request
        huespedes_serializer = HuespedesSerializer(data = request.data)
        # se validan los datos si cumplen las caracteristicas del modelo
        if huespedes_serializer.is_valid():
            # se salvan los datos enviados en la base de datos
            huespedes_serializer.save()
            # se responde la data enviada al solicitante
            return Response(huespedes_serializer.data)
        # se envia un error si los datos no cumplen con el modelo
        return Response(huespedes_serializer.errors)

#se agrega el modificador a la funcion para restringir los metodos http permitidos
@api_view(['GET','PUT','DELETE'])
def huespedes_detail_view(request,pk=None):

    # se filtran los mensajes enviados por GET
    if request.method == 'GET':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        huespedes = Huespedes.objects.filter(documento = pk).first()
        # se serializa los datos para la manipulacion
        huespedes_serializer = HuespedesSerializer(huespedes)
        # se envia respuesta al solicitante
        return Response(huespedes_serializer.data)
    
    # se filtran los mensajes enviados por PUT
    elif request.method == 'PUT':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        huespedes = Huespedes.objects.filter(documento = pk).first()
        # se serializa los datos recibidos para la manipulacion
        huespedes_serializer = HuespedesSerializer(huespedes, data = request.data)
        # se validan los datos si cumplen las caracteristicas del modelo
        if huespedes_serializer.is_valid():
            # se salvan los cambios en la base de datos
            huespedes_serializer.save()
            # se responde la data enviada al solicitante
            return Response(huespedes_serializer.data)
        # se envia un error si los datos no cumplen con el modelo
        return Response(huespedes_serializer.errors)
    
    # se filtran los mensajes enviados por PUT
    elif request.method == 'DELETE':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        huespedes = Huespedes.objects.filter(documento = pk).first()
        # se borra el dato de la base de datos
        huespedes.delete()
        # se envia mensaje al solicitante
        return Response("Eliminado..")