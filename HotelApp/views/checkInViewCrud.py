# se importan para poder generar las respuestas http
from rest_framework.response import Response
# se importa para poder consumir las vistas de ayuda 
from rest_framework.views import APIView
# se importa para poder modificar las vistas de ayuda
from rest_framework.decorators import api_view
#se importa para poder usar el modelo creado
from HotelApp.models import CheckIn,checkIn
# se importa para poder manipular el modelo con el serializador creado
from HotelApp.serializers import CheckInSerializer

#se agrega el modificador a la funcion para restringir los metodos http permitidos
@api_view(['GET','POST'])
def checkIn_api_view(request):

    # se filtran los mensajes enviados por GET
    if request.method == 'GET':
        #  usando ORM se realiza la consultaa la base de datos de todos los productos
        checkIn = CheckIn.objects.all()
        # se representa la informacion solicitada en JSON y se marca many=true para asignar anidacion
        checkIn_serializer = CheckInSerializer(checkIn,many=True)
        # se envida la respuesta serializada que se encuentra en data
        return Response(checkIn_serializer.data)

    # se filtran los mensajes enviados por POST
    elif request.method == 'POST':
        # se extraen los datos enviados en la peticion request
        checkIn_serializer = CheckInSerializer(data = request.data)
        # se validan los datos si cumplen las caracteristicas del modelo
        if checkIn_serializer.is_valid():
            # se salvan los datos enviados en la base de datos
            checkIn_serializer.save()
            # se responde la data enviada al solicitante
            return Response(checkIn_serializer.data)
        # se envia un error si los datos no cumplen con el modelo
        return Response(checkIn_serializer.errors)

#se agrega el modificador a la funcion para restringir los metodos http permitidos
@api_view(['GET','PUT','DELETE'])
def checkIn_detail_view(request,pk=None):

    # se filtran los mensajes enviados por GET
    if request.method == 'GET':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        checkIn = CheckIn.objects.filter(id = pk).first()
        # se serializa los datos para la manipulacion
        checkIn_serializer = CheckInSerializer(checkIn)
        # se envia respuesta al solicitante
        return Response(checkIn_serializer.data)
    
    # se filtran los mensajes enviados por PUT
    elif request.method == 'PUT':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        checkIn = CheckIn.objects.filter(id = pk).first()
        # se serializa los datos recibidos para la manipulacion
        checkIn_serializer = CheckInSerializer(checkIn, data = request.data)
        # se validan los datos si cumplen las caracteristicas del modelo
        if checkIn_serializer.is_valid():
            # se salvan los cambios en la base de datos
            checkIn_serializer.save()
            # se responde la data enviada al solicitante
            return Response(checkIn_serializer.data)
        # se envia un error si los datos no cumplen con el modelo
        return Response(checkIn_serializer.errors)
    
    # se filtran los mensajes enviados por PUT
    elif request.method == 'DELETE':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        checkIn = CheckIn.objects.filter(id = pk).first()
        # se borra el dato de la base de datos
        checkIn.delete()
        # se envia mensaje al solicitante
        return Response("Eliminado..")