# se importan para poder generar las respuestas http
from rest_framework.response import Response
# se importa para poder consumir las vistas de ayuda 
from rest_framework.views import APIView
# se importa para poder modificar las vistas de ayuda
from rest_framework.decorators import api_view
#se importa para poder usar el modelo creado
from HotelApp.models import Habitaciones, habitaciones
# se importa para poder manipular el modelo con el serializador creado
from HotelApp.serializers import HabitacionesSerializer

#se agrega el modificador a la funcion para restringir los metodos http permitidos
@api_view(['GET','POST'])
def habitaciones_api_view(request):

    # se filtran los mensajes enviados por GET
    if request.method == 'GET':
        #  usando ORM se realiza la consultaa la base de datos de todos los productos
        habitaciones = Habitaciones.objects.all()
        # se representa la informacion solicitada en JSON y se marca many=true para asignar anidacion
        habitaciones_serializer = HabitacionesSerializer(habitaciones,many=True)
        # se envida la respuesta serializada que se encuentra en data
        return Response(habitaciones_serializer.data)

    # se filtran los mensajes enviados por POST
    elif request.method == 'POST':
        # se extraen los datos enviados en la peticion request
        habitaciones_serializer = HabitacionesSerializer(data = request.data)
        # se validan los datos si cumplen las caracteristicas del modelo
        if habitaciones_serializer.is_valid():
            # se salvan los datos enviados en la base de datos
            habitaciones_serializer.save()
            # se responde la data enviada al solicitante
            return Response(habitaciones_serializer.data)
        # se envia un error si los datos no cumplen con el modelo
        return Response(habitaciones_serializer.errors)

#se agrega el modificador a la funcion para restringir los metodos http permitidos
@api_view(['GET','PUT','DELETE'])
def habitaciones_detail_view(request,pk=None):

    # se filtran los mensajes enviados por GET
    if request.method == 'GET':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        habitaciones = Habitaciones.objects.filter(numero_habitacion = pk).first()
        # se serializa los datos para la manipulacion
        habitaciones_serializer = HabitacionesSerializer(habitaciones)
        # se envia respuesta al solicitante
        return Response(habitaciones_serializer.data)
    
    # se filtran los mensajes enviados por PUT
    elif request.method == 'PUT':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        habitaciones = Habitaciones.objects.filter(numero_habitacion = pk).first()
        # se serializa los datos recibidos para la manipulacion
        habitaciones_serializer = HabitacionesSerializer(habitaciones, data = request.data)
        # se validan los datos si cumplen las caracteristicas del modelo
        if habitaciones_serializer.is_valid():
            # se salvan los cambios en la base de datos
            habitaciones_serializer.save()
            # se responde la data enviada al solicitante
            return Response(habitaciones_serializer.data)
        # se envia un error si los datos no cumplen con el modelo
        return Response(habitaciones_serializer.errors)
    
    # se filtran los mensajes enviados por PUT
    elif request.method == 'DELETE':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        habitaciones = Habitaciones.objects.filter(numero_habitacion = pk).first()
        # se borra el dato de la base de datos
        habitaciones.delete()
        # se envia mensaje al solicitante
        return Response("Eliminado..")