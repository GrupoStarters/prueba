# se importa las vistas para que los demas codigos puedan importarla
from .huespedesViewCrud import huespedes_api_view
from .huespedesViewCrud import huespedes_detail_view
from .habitacionHuespedView import habitaciones_api_view
from .habitacionHuespedView import habitaciones_detail_view
from .checkInViewCrud import checkIn_api_view
from .checkInViewCrud import checkIn_detail_view